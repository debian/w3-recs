Source: w3-recs
Build-Depends: debhelper (>= 7.0.0), cdbs
Build-Depends-Indep: xsltproc, libxml2-utils
Section: non-free/doc
Priority: optional
Maintainer: Colin Darie <colindarie@gmail.com>
Standards-Version: 3.9.1
Vcs-Git: git://git.debian.org/git/collab-maint/w3-recs.git
Vcs-Browser: http://git.debian.org/?p=collab-maint/w3-recs.git
Homepage: http://www.w3.org

Package: w3-recs
Architecture: all
Depends: ${misc:Depends}
Recommends: www-browser
Description: Recommendations of the World Wide Web Consortium (W3C)
 This package includes the Recommendations produced by the World Wide
 Web Consortium (W3C) standardization body in HTML format.
 .
 The included Recommendations are:
 .
  * A MathML for CSS Profile
  * Architecture of the World Wide Web, Volume One
  * Associating Style Sheets with XML documents 1.0 (Second Edition)
  * Authoring Tool Accessibility Guidelines 1.0
  * CSS Color Module Level 3
  * CSS Marquee Module Level 3
  * CSS Mobile Profile 2.0
  * CSS Multi-column Layout Module
  * CSS Namespaces Module
  * CSS Style Attributes
  * CSS TV Profile 1.0
  * CSS3 Basic User Interface Module
  * CSS3 Ruby Module
  * Canonical XML Version 1.0
  * Canonical XML Version 1.1
  * Cascading Style Sheets (CSS1) Level 1 Specification
  * Cascading Style Sheets Level 2 Revision 1 (CSS 2.1) Specification
  * Character Model for the World Wide Web 1.0: Fundamentals
  * Character Model for the World Wide Web 1.0: Resource Identifiers
  * Composite Capability/Preference Profiles (CC/PP): Structure and
  Vocabularies 1.0
  * Decryption Transform for XML Signature
  * Device Description Repository Simple API
  * Digital Signatures for Widgets
  * Document Object Model (DOM) Level 1
  * Document Object Model (DOM) Level 2 Core Specification
  * Document Object Model (DOM) Level 2 Events Specification
  * Document Object Model (DOM) Level 2 HTML Specification
  * Document Object Model (DOM) Level 2 Style Specification
  * Document Object Model (DOM) Level 2 Traversal and Range Specification
  * Document Object Model (DOM) Level 2 Views Specification
  * Document Object Model (DOM) Level 3 Core Specification
  * Document Object Model (DOM) Level 3 Load and Save Specification
  * Document Object Model (DOM) Level 3 Validation Specification
  * EMMA: Extensible MultiModal Annotation markup language
  * Efficient XML Interchange (EXI) Format 1.0
  * Element Traversal Specification
  * Exclusive XML Canonicalization Version 1.0
  * Extensible Markup Language (XML) 1.0 (Fifth Edition)
  * Extensible Markup Language (XML) 1.1 (Second Edition)
  * Extensible Stylesheet Language (XSL) Version 1.0
  * Extensible Stylesheet Language (XSL) Version 1.1
  * GRDDL Test Cases
  * Geolocation API Specification
  * Gleaning Resource Descriptions from Dialects of Languages (GRDDL)
  * HTML 3.2 Reference Specification
  * HTML 4.0 Specification
  * HTML 4.01 Specification
  * Internationalization Tag Set (ITS) Version 1.0
  * Mathematical Markup Language (MathML) 1.01 Specification
  * Mathematical Markup Language (MathML) Version 2.0 (Second Edition)
  * Mathematical Markup Language (MathML) Version 3.0
  * Media Queries
  * Mobile SVG Profiles: SVG Tiny and SVG Basic
  * Mobile Web Application Best Practices
  * Mobile Web Best Practices 1.0
  * Namespaces in XML 1.0 (Third Edition)
  * Namespaces in XML 1.1 (Second Edition)
  * OWL 2 Web Ontology Language Conformance
  * OWL 2 Web Ontology Language Direct Semantics
  * OWL 2 Web Ontology Language Document Overview
  * OWL 2 Web Ontology Language Mapping to RDF Graphs
  * OWL 2 Web Ontology Language New Features and Rationale
  * OWL 2 Web Ontology Language Primer
  * OWL 2 Web Ontology Language Profiles
  * OWL 2 Web Ontology Language Quick Reference Guide
  * OWL 2 Web Ontology Language RDF-Based Semantics
  * OWL 2 Web Ontology Language Structural Specification and Functional-Style
  Syntax
  * OWL 2 Web Ontology Language XML Serialization
  * OWL Web Ontology Language Guide
  * OWL Web Ontology Language Overview
  * OWL Web Ontology Language Reference
  * OWL Web Ontology Language Semantics and Abstract Syntax
  * OWL Web Ontology Language Test Cases
  * OWL Web Ontology Language Use Cases and Requirements
  * PICS 1.1 Label Distribution -- Label Syntax and Communication Protocols
  * PICS 1.1 Rating Services and Rating Systems -- and Their Machine Readable
  Descriptions
  * PICS Signed Labels (DSig) 1.0 Specification
  * PICSRules 1.1 Specification
  * Portable Network Graphics (PNG) Specification (Second Edition)
  * Pronunciation Lexicon Specification (PLS) Version 1.0
  * Protocol for Web Description Resources (POWDER): Description Resources
  * Protocol for Web Description Resources (POWDER): Formal Semantics
  * Protocol for Web Description Resources (POWDER): Grouping of Resources
  * QA Framework: Specification Guidelines
  * RDF Primer
  * RDF Semantics
  * RDF Test Cases
  * RDF Vocabulary Description Language 1.0: RDF Schema
  * RDF/XML Syntax Specification (Revised)
  * RDFa in XHTML: Syntax and Processing
  * RIF Basic Logic Dialect
  * RIF Core Dialect
  * RIF Datatypes and Built-Ins 1.0
  * RIF Framework for Logic Dialects
  * RIF Production Rule Dialect
  * RIF RDF and OWL Compatibility
  * Resource Description Framework (RDF) Model and Syntax Specification
  * Resource Description Framework (RDF): Concepts and Abstract Syntax
  * Resource Representation SOAP Header Block
  * Ruby Annotation
  * SKOS Simple Knowledge Organization System Reference
  * SMIL Animation
  * SOAP Message Transmission Optimization Mechanism
  * SOAP Version 1.2 Part 0: Primer (Second Edition)
  * SOAP Version 1.2 Part 1: Messaging Framework (Second Edition)
  * SOAP Version 1.2 Part 2: Adjuncts (Second Edition)
  * SOAP Version 1.2 Specification Assertions and Test Collection (Second
  Edition)
  * SPARQL Protocol for RDF
  * SPARQL Query Language for RDF
  * SPARQL Query Results XML Format
  * Scalable Vector Graphics (SVG) 1.0 Specification
  * Scalable Vector Graphics (SVG) 1.1 Specification
  * Scalable Vector Graphics (SVG) Tiny 1.2 Specification
  * Selectors API Level 1
  * Selectors Level 3
  * Semantic Annotations for WSDL and XML Schema
  * Semantic Interpretation for Speech Recognition (SISR) Version 1.0
  * Service Modeling Language Interchange Format Version 1.1
  * Service Modeling Language, Version 1.1
  * Speech Recognition Grammar Specification Version 1.0
  * Speech Synthesis Markup Language (SSML) Version 1.0
  * Speech Synthesis Markup Language (SSML) Version 1.1
  * Synchronized Multimedia Integration Language (SMIL 2.1)
  * Synchronized Multimedia Integration Language (SMIL 3.0)
  * Synchronized Multimedia Integration Language (SMIL) 1.0 Specification
  * The 'view-mode' Media Feature
  * The Platform for Privacy Preferences 1.0 (P3P1.0) Specification
  * Timed Text Markup Language (TTML) 1.0
  * User Agent Accessibility Guidelines 1.0
  * Voice Browser Call Control: CCXML Version 1.0
  * Voice Extensible Markup Language (VoiceXML) 2.1
  * Voice Extensible Markup Language (VoiceXML) Version 2.0
  * W3C XML Schema Definition Language (XSD): Component Designators
  * W3C mobileOK Basic Tests 1.0
  * Web Content Accessibility Guidelines (WCAG) 2.0
  * Web Content Accessibility Guidelines 1.0
  * Web Security Context: User Interface Guidelines
  * Web Services Addressing 1.0 - Core
  * Web Services Addressing 1.0 - Metadata
  * Web Services Addressing 1.0 - SOAP Binding
  * Web Services Choreography Description Language Version 1.0
  * Web Services Description Language (WSDL) Version 2.0 Part 0: Primer
  * Web Services Description Language (WSDL) Version 2.0 Part 1: Core Language
  * Web Services Description Language (WSDL) Version 2.0 Part 2: Adjuncts
  * Web Services Policy 1.5 - Attachment
  * Web Services Policy 1.5 - Framework
  * WebCGM 1.0 Second Release
  * WebCGM 2.0
  * WebCGM 2.1
  * Widget Access Request Policy
  * XForms 1.0 Basic Profile
  * XForms 1.1
  * XHTML-Print - Second Edition
  * XHTML™ 1.0 The Extensible HyperText Markup Language (Second Edition)
  * XHTML™ 1.1 - Module-based XHTML - Second Edition
  * XHTML™ Basic 1.1 - Second Edition
  * XHTML™ Modularization 1.1 - Second Edition
  * XML Base (Second Edition)
  * XML Binding Language (XBL) 2.0
  * XML Entity Definitions for Characters
  * XML Events
  * XML Fragment Interchange
  * XML Inclusions (XInclude) Version 1.0 (Second Edition)
  * XML Information Set (Second Edition)
  * XML Key Management Specification (XKMS 2.0)
  * XML Key Management Specification (XKMS 2.0) Bindings
  * XML Linking Language (XLink) Version 1.0
  * XML Linking Language (XLink) Version 1.1
  * XML Path Language (XPath) 2.0
  * XML Path Language (XPath) 2.0 (Second Edition)
  * XML Path Language (XPath) Version 1.0
  * XML Schema Part 0: Primer Second Edition
  * XML Schema Part 1: Structures Second Edition
  * XML Schema Part 2: Datatypes Second Edition
  * XML Signature Syntax and Processing (Second Edition)
  * XML Syntax for XQuery 1.0 (XQueryX)
  * XML Syntax for XQuery 1.0 (XQueryX) (Second Edition)
  * XML-Signature XPath Filter 2.0
  * XML-binary Optimized Packaging
  * XMLHttpRequest
  * XPointer Framework
  * XPointer element() Scheme
  * XPointer xmlns() Scheme
  * XProc: An XML Pipeline Language
  * XQuery 1.0 and XPath 2.0 Data Model (XDM) (Second Edition)
  * XQuery 1.0 and XPath 2.0 Formal Semantics (Second Edition)
  * XQuery 1.0 and XPath 2.0 Functions and Operators (Second Edition)
  * XQuery 1.0: An XML Query Language
  * XQuery 1.0: An XML Query Language (Second Edition)
  * XQuery Update Facility 1.0
  * XQuery Update Facility 1.0 Requirements
  * XQuery Update Facility 1.0 Use Cases
  * XQuery and XPath Full Text 1.0
  * XSL Transformations (XSLT) Version 1.0
  * XSL Transformations (XSLT) Version 2.0
  * XSLT 2.0 and XQuery 1.0 Serialization (Second Edition)
  * rdf:PlainLiteral: A Datatype for RDF Plain Literals
  * xml:id Version 1.0

