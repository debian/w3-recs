<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:exslt="http://exslt.org/common"
    xmlns:str="http://exslt.org/strings"
    xmlns:contact="http://www.w3.org/2000/10/swap/pim/contact#"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:doc="http://www.w3.org/2000/10/swap/pim/doc#"
    xmlns:mat="http://www.w3.org/2002/05/matrix/vocab#"
    xmlns:org="http://www.w3.org/2001/04/roadmap/org#"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:rec="http://www.w3.org/2001/02pd/rec54#"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:html="http://www.w3.org/1999/xhtml">

  <xsl:import href="w3-tr-lib.xsl" />
  <xsl:output mode="xml" encoding="utf-8" />

  <xsl:param name="basepath"></xsl:param>

  <xsl:template match="/">
    <xsl:variable name="trs"> <!-- selected technical reports -->
      <xsl:call-template name="find_trs" />
    </xsl:variable>

    <html>
      <head>
        <title>W3C Recommendation List</title>
      </head>
      <body>
        <p>Below you can find a list of the <a
            href="http://www.w3.org">W3C</a> recommendations installed on this
          system.</p>
        <ul>
        <xsl:apply-templates select="exslt:node-set($trs)/*">
	  <xsl:sort select="normalize-space(dc:title)" />
	</xsl:apply-templates>
        </ul>
      </body>
    </html>

  </xsl:template>

  <xsl:template match="rec:REC|rec:PR|rec:CR">
    <xsl:variable name="title">
      <xsl:call-template name="title_of_tr">
        <xsl:with-param name="rec" select="." />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="docpath">
      <xsl:call-template name="path_of_tr">
        <xsl:with-param name="rec" select="." />
        <xsl:with-param name="basepath" select="'.'" />
      </xsl:call-template>
    </xsl:variable>

    <li>
      <a href="{str:replace($docpath, './html/', '')}index.html">
	<xsl:value-of select="$title" />
      </a>
    </li>
  </xsl:template>

</xsl:stylesheet>
