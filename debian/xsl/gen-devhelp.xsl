<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:exslt="http://exslt.org/common"
    xmlns:str="http://exslt.org/strings"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:rec="http://www.w3.org/2001/02pd/rec54#">

  <xsl:import href="w3-tr-lib.xsl" />
  <xsl:output mode="xml" encoding="utf-8" />

  <xsl:param name="version"></xsl:param>

  <xsl:template match="/">
    <xsl:variable name="trs"> <!-- selected technical reports -->
      <xsl:call-template name="find_trs" />
    </xsl:variable>

    <book title="W3C Recommendations" name="W3C Recs" version="{$version}" link="index.html">
      <chapters>
        <xsl:apply-templates select="exslt:node-set($trs)/*">
	  <xsl:sort select="normalize-space(dc:title)" />
	</xsl:apply-templates>
      </chapters>
    </book>
  </xsl:template>

  <xsl:template match="rec:REC|rec:PR|rec:CR">
    <xsl:variable name="title">
      <xsl:call-template name="title_of_tr">
        <xsl:with-param name="rec" select="." />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="docpath">
      <xsl:call-template name="path_of_tr">
        <xsl:with-param name="rec" select="." />
        <xsl:with-param name="basepath" select="'.'" />
      </xsl:call-template>
    </xsl:variable>

    <sub link="{str:replace($docpath, './html/', '')}index.html" name="{$title}" />
  </xsl:template>

</xsl:stylesheet>
