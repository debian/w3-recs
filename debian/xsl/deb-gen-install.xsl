<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:exslt="http://exslt.org/common"
    xmlns:contact="http://www.w3.org/2000/10/swap/pim/contact#"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:doc="http://www.w3.org/2000/10/swap/pim/doc#"
    xmlns:mat="http://www.w3.org/2002/05/matrix/vocab#"
    xmlns:org="http://www.w3.org/2001/04/roadmap/org#"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:rec="http://www.w3.org/2001/02pd/rec54#">

  <xsl:import href="w3-tr-lib.xsl" />
  <xsl:output method="text" />

  <xsl:param name="basepath"></xsl:param>

  <xsl:template match="/">
    <xsl:variable name="trs"> <!-- selected technical reports -->
      <xsl:call-template name="find_trs" />
    </xsl:variable>

    <xsl:for-each select="exslt:node-set($trs)/*">
      <xsl:variable name="id">
        <xsl:call-template name="id_of_tr">
          <xsl:with-param name="rec" select="." />
        </xsl:call-template>
      </xsl:variable>

      <xsl:text>RECs/</xsl:text>
      <xsl:value-of select="$id" />
      <xsl:text>/</xsl:text>
      <xsl:text>&#x09;</xsl:text> <!-- TAB -->
      <xsl:value-of select="$basepath" />
      <xsl:text>/</xsl:text>
      <xsl:text>&#x0A;</xsl:text>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
