<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:str="http://exslt.org/strings"
    xmlns:contact="http://www.w3.org/2000/10/swap/pim/contact#"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:doc="http://www.w3.org/2000/10/swap/pim/doc#"
    xmlns:mat="http://www.w3.org/2002/05/matrix/vocab#"
    xmlns:org="http://www.w3.org/2001/04/roadmap/org#"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:rec="http://www.w3.org/2001/02pd/rec54#">

    <!-- node deep copy -->
  <xsl:template name="deep_copy">
    <xsl:copy>
      <xsl:for-each select="@*|node()">
        <xsl:call-template name="deep_copy" />
      </xsl:for-each>
    </xsl:copy>
  </xsl:template>

    <!-- implement the policy of selecting which TRs we are interested in. ATM
    we keep Recommendations (rec:REC elements) and throw away the rest -->
  <xsl:template name="find_trs">
    <xsl:for-each select="//rec:REC|//rec:PR|//rec:CR">
      <xsl:call-template name="deep_copy" />
    </xsl:for-each>
  </xsl:template>

    <!-- extract an unique identifier for a given TR element. Current policy is
    to use the last part of the URL, so for a TR with
    rdf:about="http://www.w3.org/TR/2004/REC-xml11-20040204" the id will be
    "REC-xml11-20040204" -->
  <xsl:template name="id_of_tr">
    <xsl:param name="rec"></xsl:param>
    <!-- "http://www.w3.org/TR/2004/REC-xml11-20040204" -> ("REC", "xml11", "20040204") -->
    <xsl:variable name="pieces" select="str:tokenize(
        str:tokenize($rec/@rdf:about, '/')[position()=last()],
        '-')" />
    <xsl:for-each select="$pieces"> <!-- concatenate pieces with '-' -->
      <xsl:value-of select="string(.)" />
      <xsl:if test="position()!=last()">
        <xsl:text>-</xsl:text>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

    <!-- given a technical report element filter on its title normalizing the
    value -->
  <xsl:template name="title_of_tr">
    <xsl:param name="rec"></xsl:param>
    <xsl:value-of select="normalize-space($rec/dc:title)" />
  </xsl:template>

  <xsl:template name="path_of_tr">
    <xsl:param name="rec"></xsl:param>
    <xsl:param name="basepath"></xsl:param>
    <xsl:variable name="path">
      <xsl:value-of select="$basepath" />
      <xsl:text>/html/</xsl:text>
      <xsl:value-of select="substring-after($rec/@rdf:about, 'http://')" />
    </xsl:variable>
    <xsl:value-of select="$path" />
    <xsl:if test="substring($path, string-length($path), 1) != '/'">  <!-- add trailing "/" if needed -->
      <xsl:text>/</xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template name="editors_of_tr">
    <xsl:param name="rec"></xsl:param>
    <xsl:for-each select="$rec/rec:editor/contact:fullName">
      <xsl:value-of select="string(.)" />
      <xsl:if test="position()!=last()">
        <xsl:text>, </xsl:text>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
