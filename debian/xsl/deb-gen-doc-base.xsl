<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:exslt="http://exslt.org/common"
    xmlns:contact="http://www.w3.org/2000/10/swap/pim/contact#"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:doc="http://www.w3.org/2000/10/swap/pim/doc#"
    xmlns:mat="http://www.w3.org/2002/05/matrix/vocab#"
    xmlns:org="http://www.w3.org/2001/04/roadmap/org#"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:rec="http://www.w3.org/2001/02pd/rec54#">

  <xsl:import href="w3-tr-lib.xsl" />
  <xsl:output method="text" />

  <xsl:param name="basepath"></xsl:param>
  <xsl:param name="section"></xsl:param>
  <xsl:param name="format"></xsl:param>

  <xsl:template match="/">
    <xsl:variable name="trs"> <!-- selected technical reports -->
      <xsl:call-template name="find_trs" />
    </xsl:variable>

    <xsl:apply-templates select="exslt:node-set($trs)/*" />
  </xsl:template>

  <xsl:template match="rec:REC|rec:PR|rec:CR">
    <xsl:variable name="id">
      <xsl:call-template name="id_of_tr">
        <xsl:with-param name="rec" select="." />
      </xsl:call-template>
    </xsl:variable>

    <!-- lower case version of $id
         needed because doc-base Document IDs can't contain upper case letters -->
    <xsl:variable name="lid">
      <xsl:value-of select="translate($id,
	'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
    </xsl:variable>

    <xsl:variable name="title">
      <xsl:call-template name="title_of_tr">
        <xsl:with-param name="rec" select="." />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="editors">
      <xsl:call-template name="editors_of_tr">
        <xsl:with-param name="rec" select="." />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="docpath">
      <xsl:call-template name="path_of_tr">
	<xsl:with-param name="rec" select="." />
	<xsl:with-param name="basepath" select="$basepath" />
      </xsl:call-template>
    </xsl:variable>

    <xsl:text>&lt;&lt;&lt;&lt; </xsl:text>  <!-- cut marker -->
    <xsl:value-of select="$lid" />
    <xsl:text> &lt;&lt;&lt;&lt;</xsl:text>
    <xsl:text>&#x0A;</xsl:text>

    <xsl:text>Document: w3-recs-</xsl:text> <xsl:value-of select="$lid" /> <xsl:text>&#x0A;</xsl:text>
    <xsl:text>Title: </xsl:text> <xsl:value-of select="$title" /> <xsl:text>&#x0A;</xsl:text>
    <xsl:text>Author: </xsl:text> <xsl:value-of select="$editors" /> <xsl:text>&#x0A;</xsl:text>
    <xsl:text>Section: </xsl:text> <xsl:value-of select="$section" /> <xsl:text>&#x0A;</xsl:text>
    <xsl:text>&#x0A;</xsl:text>

    <xsl:text>Format: </xsl:text> <xsl:value-of select="$format" /> <xsl:text>&#x0A;</xsl:text>

    <xsl:text>Index: </xsl:text>
    <xsl:value-of select="$docpath" />
    <xsl:text>index.html</xsl:text>
    <xsl:text>&#x0A;</xsl:text>

    <xsl:text>Files: </xsl:text>
    <xsl:value-of select="$docpath" />
    <xsl:text>*</xsl:text>
    <xsl:text>&#x0A;</xsl:text>

  </xsl:template>

</xsl:stylesheet>
