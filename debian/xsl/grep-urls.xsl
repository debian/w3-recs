<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:exslt="http://exslt.org/common"
    xmlns:contact="http://www.w3.org/2000/10/swap/pim/contact#"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:doc="http://www.w3.org/2000/10/swap/pim/doc#"
    xmlns:mat="http://www.w3.org/2002/05/matrix/vocab#"
    xmlns:org="http://www.w3.org/2001/04/roadmap/org#"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:rec="http://www.w3.org/2001/02pd/rec54#">

  <xsl:import href="w3-tr-lib.xsl" />
  <xsl:output method="text" />

  <xsl:template match="/">
    <xsl:variable name="trs"> <!-- selected technical reports -->
      <xsl:call-template name="find_trs" />
    </xsl:variable>

    <xsl:for-each select="exslt:node-set($trs)/*">  <!-- project on URL and output -->
      <xsl:variable name="url">
        <xsl:value-of select="@rdf:about" />
      </xsl:variable>
      <xsl:value-of select="$url" />
      <xsl:if test="substring($url, string-length($url), 1) != '/'">  <!-- add trailing "/" if needed -->
        <xsl:text>/</xsl:text>
      </xsl:if>
      <xsl:text>&#x0A;</xsl:text>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
